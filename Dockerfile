FROM anapsix/alpine-java:8
ADD target/io-server-rawdata.jar io-server-rawdata.jar
ENV JAVA_OPTS=""
ENTRYPOINT exec java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /io-server-rawdata.jar