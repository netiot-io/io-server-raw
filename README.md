# IO-SERVER-RAW
Microservice for saving the raw data from devices (pre-decoding).

## DB
Uses cassandra for database. Init script in `src/main/resources/cassandra.cql`.

## Kafka
Listens for kafka messages from the devices.

## CI/CD
Compile, test, package, build docker image and push to gitlab docker registry on the topic from the config. 

## Tests
Groovy tests in `src/test/groovy`.