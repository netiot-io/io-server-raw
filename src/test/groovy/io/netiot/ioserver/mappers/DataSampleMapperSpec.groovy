package io.netiot.ioserver.mappers

import spock.lang.Specification

import static io.netiot.ioserver.generators.DataSampleGenerator.aDataSample
import static io.netiot.ioserver.generators.DataSampleGenerator.aDataSampleModel

class DataSampleMapperSpec extends Specification {

    def 'toEntity'() {
        given:
        def entity = aDataSample()
        def model = aDataSampleModel()

        when:
        def result = DataSampleMapper.toEntity(model)

        then:
        result == entity
    }

    def 'toModel'() {
        given:
        def entity = aDataSample()

        when:
        def result = DataSampleMapper.toModel(entity)

        then:
        result != null
        result.timestamp == entity.timestamp.getTime()
    }

}
