package io.netiot.ioserver.generators

import io.netiot.ioserver.models.DataSampleEvent

class DataEventGenerator {

    static aDataEvent(Map overrides = [:]) {
        Map values = [
                devices : [DeviceDataGenerator.aDeviceDataSample()],
        ]
        values << overrides
        return DataSampleEvent.newInstance(values)
    }

}
