package io.netiot.ioserver.generators


import io.netiot.ioserver.models.DeviceData

class DeviceDataGenerator {
    static aDeviceDataSample(Map overrides = [:]) {
        Map values = [
                deviceId        : 1L,
                timestamp : 1531055804140L,
                rawdata    : "values",
        ]
        values << overrides
        return DeviceData.newInstance(values)
    }
}
