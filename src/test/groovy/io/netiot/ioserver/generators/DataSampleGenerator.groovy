package io.netiot.ioserver.generators

import io.netiot.ioserver.entities.DataSample
import io.netiot.ioserver.models.DataSampleModel

class DataSampleGenerator {

    static aDataSample(Map overrides = [:]) {
        Map values = [
                deviceId        : 1L,
                year      : 2018,
                month     : 7,
                timestamp : new Date(1531055804140),
                rawdata    : "values",
        ]
        values << overrides
        return DataSample.newInstance(values)
    }

    static aDataSampleModel(Map overrides = [:]) {
        Map values = [
                deviceId        : 1L,
                timestamp : 1531055804140,
                rawdata    : "values"
        ]
        values << overrides
        return DataSampleModel.newInstance(values)
    }

}
