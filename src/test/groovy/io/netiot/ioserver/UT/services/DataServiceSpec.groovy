package io.netiot.ioserver.UT.services

import com.fasterxml.jackson.databind.JsonNode

import io.netiot.ioserver.generators.DataSampleGenerator
import io.netiot.ioserver.services.DataService
import org.springframework.data.cassandra.core.CassandraOperations
import org.springframework.data.cassandra.core.InsertOptions
import spock.lang.Specification

import static io.netiot.ioserver.generators.DataEventGenerator.aDataEvent

class DataServiceSpec extends Specification {

    def cassandraOperations = Mock(CassandraOperations)
    def dataService = new DataService(cassandraOperations)

    def 'saveData'(){
        given:
        def dataEvent = aDataEvent()
        def dataSample = DataSampleGenerator.aDataSample()

        when:
        dataService.saveData(dataEvent)

        then:
        1 * cassandraOperations.insert(dataSample, _ as InsertOptions)
        0 * _
    }

    def 'generateList'(){
        given:
        def startNumber = 1
        def limit = 5
        Set numbers = [1, 2, 3, 4, 5]

        when:
        def list = dataService.generateList(startNumber, limit)

        then:
        list == numbers
    }
}
