package io.netiot.ioserver;

import io.netiot.ioserver.configurations.KafkaChannels;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.stream.annotation.EnableBinding;

@EnableBinding(KafkaChannels.class)
@EnableDiscoveryClient
@SpringBootApplication
public class IoServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(IoServerApplication.class, args);
	}
}
