package io.netiot.ioserver.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

import java.util.Date;

@Table(value = "raw_data")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DataSample {

    @PrimaryKeyColumn(name = "deviceId", ordinal = 0, type = PrimaryKeyType.PARTITIONED)
    private Long deviceId;

    @PrimaryKeyColumn(name = "year", ordinal = 1, type = PrimaryKeyType.PARTITIONED)
    private Integer year;

    @PrimaryKeyColumn(name = "month", ordinal = 2, type = PrimaryKeyType.PARTITIONED)
    private Integer month;

    @PrimaryKeyColumn(name = "timestamp", ordinal = 3, type = PrimaryKeyType.CLUSTERED)
    private Date timestamp;

    @Column(value = "rawdata")
    private String rawdata;

}
