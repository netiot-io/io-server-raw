package io.netiot.ioserver.mappers;

import io.netiot.ioserver.entities.DataSample;
import io.netiot.ioserver.models.DataSampleEvent;
import io.netiot.ioserver.models.DeviceData;
import io.netiot.ioserver.models.DataSampleModel;

import java.time.Instant;
import java.time.LocalDateTime;

import java.time.ZoneOffset;
import java.util.List;
import java.util.Date;
import java.util.stream.Collectors;


public final class DataSampleMapper {

    private DataSampleMapper() {
    }

    public static DataSample toEntity(final DataSampleModel dataSampleModel) {
        LocalDateTime timestamp = Instant.ofEpochMilli(dataSampleModel.getTimestamp())
                .atZone(ZoneOffset.UTC).toLocalDateTime();
        return DataSample.builder()
                .deviceId(dataSampleModel.getDeviceId())
                .month(timestamp.getMonthValue())
                .year(timestamp.getYear())
                .timestamp(new Date(dataSampleModel.getTimestamp()))
                .rawdata(dataSampleModel.getRawdata())
                .build();
    }

    private static DataSample toEntity(final DeviceData deviceData) {
        LocalDateTime timestamp = Instant.ofEpochMilli(deviceData.getTimestamp())
                .atZone(ZoneOffset.UTC).toLocalDateTime();
        return DataSample.builder()
                .deviceId(deviceData.getDeviceId())
                .month(timestamp.getMonthValue())
                .year(timestamp.getYear())
                .timestamp(new Date(deviceData.getTimestamp()))
                .rawdata(deviceData.getRawdata().toString())
                .build();
    }

    public static List<DataSample> toEntities(final DataSampleEvent dataSampleEvent) {
        return dataSampleEvent.getDevices().stream().map(x -> DataSampleMapper.toEntity(x)).collect(Collectors.toList());
    }

    public static DataSampleModel toModel(final DataSample dataSample){
        return DataSampleModel.builder()
                .deviceId(dataSample.getDeviceId())
                .timestamp(dataSample.getTimestamp().getTime())
                .rawdata(dataSample.getRawdata())
                .build();
    }
}
