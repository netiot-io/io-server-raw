package io.netiot.ioserver.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DeviceData
{
    private Long deviceId;

    private Long timestamp;

    @JsonProperty("data")
    private String rawdata;
}
