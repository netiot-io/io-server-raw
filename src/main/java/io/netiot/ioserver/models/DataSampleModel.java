package io.netiot.ioserver.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DataSampleModel {

    private Long deviceId;

    private Long timestamp;

    private String rawdata;
}
