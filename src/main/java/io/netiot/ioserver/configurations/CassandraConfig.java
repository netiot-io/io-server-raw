package io.netiot.ioserver.configurations;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.cassandra.config.AbstractCassandraConfiguration;
import org.springframework.data.cassandra.config.CassandraClusterFactoryBean;

import java.util.Objects;

@Configuration
public class CassandraConfig extends AbstractCassandraConfiguration {


    private final String hosts;
    private final Integer port;
    private final String username;
    private final String password;

    public CassandraConfig(@Value("${spring.data.cassandra.contact-points}") final String host,
                           @Value("${spring.data.cassandra.port}") final Integer port,
                           @Value("${spring.data.cassandra.username}") final String username,
                           @Value("${spring.data.cassandra.password}") final String password) {
        this.hosts = Objects.requireNonNull(host,"spring.data.cassandra.contact-points must be not null.");
        this.port = Objects.requireNonNull(port,"spring.data.cassandra.port must be not null.");
        this.username = Objects.requireNonNull(username,"spring.data.cassandra.username must be not null.");
        this.password = Objects.requireNonNull(password,"spring.data.cassandra.password must be not null.");
    }

    @Override
    protected String getKeyspaceName() {
        return "netiot";
    }

    @Bean
    public CassandraClusterFactoryBean cluster() {
        CassandraClusterFactoryBean cluster =
                new CassandraClusterFactoryBean();
        cluster.setContactPoints(hosts);
        cluster.setPort(port);
        cluster.setUsername(username);
        cluster.setPassword(password);
        cluster.setJmxReportingEnabled(false);
        return cluster;
    }

}
