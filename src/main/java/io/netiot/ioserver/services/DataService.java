package io.netiot.ioserver.services;

import com.datastax.driver.core.ConsistencyLevel;
import io.netiot.ioserver.mappers.DataSampleMapper;
import io.netiot.ioserver.models.DataSampleEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.cassandra.core.CassandraOperations;
import org.springframework.data.cassandra.core.InsertOptions;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class DataService {

    private final CassandraOperations cassandraOperations;

    public void saveData(final DataSampleEvent dataSampleEvent) {
        InsertOptions insertOptions = InsertOptions.builder()
                .consistencyLevel(ConsistencyLevel.LOCAL_QUORUM)
                .build();

        DataSampleMapper.toEntities(dataSampleEvent).forEach(x -> cassandraOperations.insert(x, insertOptions));
    }

    protected Set<Integer> generateList(final Integer startNumber, final Integer limit) {
        return Stream.iterate(startNumber, i -> i + 1).limit(limit).collect(Collectors.toSet());
    }
}
