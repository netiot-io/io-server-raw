package io.netiot.ioserver.services;

import io.netiot.ioserver.configurations.KafkaChannels;
import io.netiot.ioserver.models.DataSampleEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class KafkaMessageReceiverService {

    private final DataService dataService;

    @StreamListener(KafkaChannels.DATA_INPUT)
    public void receiveEvent(final DataSampleEvent dataSampleEvent) {
        log.info("Received: " + dataSampleEvent.toString());
        dataService.saveData(dataSampleEvent);
    }
}
